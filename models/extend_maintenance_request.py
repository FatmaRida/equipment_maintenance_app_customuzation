from odoo import models, fields


class MaintenanceChecklist(models.Model):
    _inherit = 'maintenance.request'

    company_id = fields.Many2one('res.company', string='Company')
    product_id = fields.Many2one('product.product',string='Product')
    partner_id = fields.Many2one('res.partner', string='Customer')
    description = fields.Text('Description')
    product_uom_qty = fields.Float(string='Quantity')
    tax_ids = fields.Many2many('account.tax', string='Taxes')
    price_unit = fields.Float(string='Unit Price')
    note = fields.Text()

    sales_order_count = fields.Integer(compute='get_sales_order_count')
    # method for counting the number of orders
    def get_sales_order_count(self):
        count = self.env['sale.order'].search_count([('partner_id','=',self.id)])
        self.sales_order_count=count

    state = fields.Selection([
        ('new_request', 'New Request'),
        ('first_approval', 'First Approval'),
        ('second_approval', 'Second Approval'),
        ('in_progress', 'In Progress'),
        ('repaired', 'Repaired'),
        ('scrap','Scrap'),
    ],default='new_request')
