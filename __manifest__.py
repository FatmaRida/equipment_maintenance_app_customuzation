# -*- coding: utf-8 -*-
{
    'name': "equipment_maintenance_app",

    'summary': """
    equipment_maintenance_app""",

    'description': """
        equipment_maintenance_app
    """,

    'author': "FatmaRida && zainb",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'maintenance_app',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','maintenance','account','stock','hr','purchase','project','website','sale'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/extend_maintenance_request.xml',
        # 'views/templates.xml',
        'views/maintenance_checklist.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
